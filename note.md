UberPigeon

Entities:

```yaml
pigeons:
    attributes:
        id:
            type:
                - integer
                - primary key
            description: Unique ID for this entity.

        created_at:
            type:
                - timestamp
                - nullable
            description: Created timestamp.

        updated_at:
            type:
                - timestamp
                - nullable
            description: Created timestamp.

        name:
            type:
                - string
            description: Name of pigeon.

        speed:
            type:
                - integer
                - unsigned
            description: The flying speed in KM/H.

        range:
            type: 
                - integer
                - unsigned
            description: Maximum range in KM.

        cost:
            type:
                - integer
                - unsigned
            description: Fixed cost each KM.

        downtime:
            type:
                - integer
                - unsigned
            description: The time it needs to rest between two deliveries in hours.

        on_order:
            type:
                - boolean
                - default false
            description: Flag for is pigeon on order.    

        rested_at:
            type:
                - timestamp
                - nullable
            description: Start timestamp pigeon rest.

orders:
    attributes:
        id:
            type:
                - integer
                - primary key
            description: Unique ID for this entity.

        created_at:
            type:
                - timestamp
                - nullable
            description: Created timestamp.

        updated_at:
            type:
                - timestamp
                - nullable
            description: Created timestamp.

        pigeon_id:
            type:
                - integer
                - unsigned
                - nullable
                - reference to pigeons.id
            description: Foreign ID to get pigeon.

        status:
            type:
                - string
                - enums:
                    - rejected
                    - placed
                    - completed
            description: Status of order.

        customer_name:
            type:
                - string
            description: Customer name.

        customer_phone:
            type:
                - string
            description: Customer phone.

        distance:
            type:
                - integer
                - unsigned
            description: Order distance in KM.

        deadline_at:
            type:
                - timestamp
            description: Order deadline need to be finished.

        price:
            type:
                - integer
                - unsigned
                - nullable
            description: Calculated price when order status is placed.
```

Business Logic:

- The customer submit an order containing distance and deadline.
- UberPigeon™ determined and rejects the order if it is not possible to deliver before the deadline.
- When the order is received, UberPigeon™ will calculate the cost for further invoicing and save the order.

REST API:

- POST /v1/orders
- GET /v1/orders
- PATCH /v1/orders/{orderId}/status/completed