<?php

namespace Tests\Unit\Actions;

use App\Contracts\Actions\RetrievePaginatedOrdersAction;
use App\Contracts\DataTransferObjects\RetrievePaginatedOrdersQueryDto;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RetrievePaginatedOrdersActionTest extends TestCase
{
    use RefreshDatabase;

    protected RetrievePaginatedOrdersAction $retrievePaginatedOrdersAction;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        Order::factory(10)->create();

        $this->retrievePaginatedOrdersAction = app(RetrievePaginatedOrdersAction::class);
    }

    /**
     * @dataProvider sampleDtos
     * @param RetrievePaginatedOrdersQueryDto $retrieveOrdersQueryDto
     * @return void
     */
    public function test_should_returns_paginated_orders(RetrievePaginatedOrdersQueryDto $retrieveOrdersQueryDto): void
    {
        /** @var LengthAwarePaginator */
        $orders = $this->retrievePaginatedOrdersAction->run($retrieveOrdersQueryDto);

        $this->assertGreaterThan(0, $orders->total());
        $this->assertInstanceOf(LengthAwarePaginator::class, $orders);
        $this->assertEquals($retrieveOrdersQueryDto->getPage(), $orders->currentPage());
        $this->assertEquals($retrieveOrdersQueryDto->getPerPage(), $orders->perPage());

        if ($retrieveOrdersQueryDto->getStatus() && count($orders->items())) {
            $this->assertEquals($retrieveOrdersQueryDto->getStatus(), $orders->items()[0]->status);
        }
    }

    /**
     * @return array
     */
    public function sampleDtos(): array
    {
        return [
            'page 1, per_page 2, and status placed' => [
                (new RetrievePaginatedOrdersQueryDto())
                    ->setPage(1)
                    ->setPerPage(2)
                    ->setStatus(OrderStatusEnum::PLACED)
            ],
            'page 2, per_page 3, and status rejected' => [
                (new RetrievePaginatedOrdersQueryDto())
                    ->setPage(2)
                    ->setPerPage(3)
                    ->setStatus(OrderStatusEnum::REJECTED)
            ],
            'default' => [
                new RetrievePaginatedOrdersQueryDto()
            ],
        ];
    }
}
