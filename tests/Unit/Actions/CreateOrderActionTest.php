<?php

namespace Tests\Unit\Actions;

use App\Contracts\Actions\CreateOrderAction;
use App\Contracts\DataTransferObjects\CreateOrderDto;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CreateOrderActionTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    protected CreateOrderAction $createOrderAction;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $this->createOrderAction = app(CreateOrderAction::class);
    }

    /**
     * @dataProvider acceptableData
     * @param CreateOrderDto $createOrderDto
     * @return void
     */
    public function test_should_return_order_with_status_is_placed_when_receive_acceptable_data(CreateOrderDto $createOrderDto): void
    {
        $order = $this->createOrderAction->run($createOrderDto);

        $this->assertCreateOrderDtoDataSavedToOrder($createOrderDto, $order);
        $this->assertEquals(OrderStatusEnum::PLACED, $order->status);
        $this->assertNotNull($order->pigeon);
        $this->assertEquals($order->pigeon->cost * $order->distance, $order->price);
        $this->assertTrue($order->pigeon->on_order);
    }

    public function acceptableData(): array
    {
        return [
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    600,
                    Carbon::now()->addHours(9)
                ),
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    500,
                    Carbon::now()->addHours(7)
                )
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    1000,
                    Carbon::now()->addHours(16)
                )
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    800,
                    Carbon::now()->addHours(12)
                )
            ],
        ];
    }

    /**
     * @dataProvider nonAcceptableData
     * @param CreateOrderDto $createOrderDto
     * @return void
     */
    public function test_should_return_order_with_status_is_rejected_when_receive_non_acceptable_data(CreateOrderDto $createOrderDto): void
    {
        $order = $this->createOrderAction->run($createOrderDto);

        $this->assertCreateOrderDtoDataSavedToOrder($createOrderDto, $order);
        $this->assertEquals(OrderStatusEnum::REJECTED, $order->status);
        $this->assertNull($order->pigeon);
        $this->assertNull($order->price);
    }

    public function nonAcceptableData(): array
    {
        return [
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    600,
                    Carbon::now()->addHours(7)
                ),
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    500,
                    Carbon::now()->addHours(6)
                )
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    1000,
                    Carbon::now()->addHours(15)
                )
            ],
            [
                new CreateOrderDto(
                    'John Doe',
                    '6281123123123',
                    800,
                    Carbon::now()->addHours(11)
                )
            ],
        ];
    }

    public function assertCreateOrderDtoDataSavedToOrder(CreateOrderDto $createOrderDto, Order $order)
    {
        $this->assertEquals($createOrderDto->customer_name, $order->customer_name);
        $this->assertEquals($createOrderDto->customer_phone, $order->customer_phone);
        $this->assertEquals($createOrderDto->distance, $order->distance);
        $this->assertEqualsWithDelta($createOrderDto->deadline_at->getTimestamp(), $order->deadline_at->getTimestamp(), 5);
    }
}
