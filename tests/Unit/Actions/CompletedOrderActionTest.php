<?php

namespace Tests\Unit\Actions;

use App\Contracts\Actions\CompletedOrderAction;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use App\Models\Pigeon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CompletedOrderActionTest extends TestCase
{
    use RefreshDatabase;

    protected CompletedOrderAction $completedOrderAction;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $this->completedOrderAction = app(CompletedOrderAction::class);
    }

    public function commonOrderData(): array
    {
        return [
            [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 500,
                    'deadline_at' => Carbon::now()->addHours(10),
                ],
            ]
        ];
    }

    /**
     * @dataProvider commonOrderData
     * @param array $commonOrderData
     * @return void
     */
    public function test_should_update_order_and_pigeon_status(array $commonOrderData): void
    {
        /** @var Pigeon */
        $pigeon = Pigeon::first();

        /** @var Order */
        $placedOrder = Order::create($commonOrderData + [
            'status' => OrderStatusEnum::PLACED,
            'pigeon_id' => $pigeon->id,
            'price' => $pigeon->cost * $commonOrderData['distance'],
        ]);

        $order = $this->completedOrderAction->run($placedOrder->id);

        $this->assertEquals(OrderStatusEnum::COMPLETED, $order->status);
        $this->assertFalse($order->pigeon->on_order);
        $this->assertEqualsWithDelta(
            Carbon::now()->getTimestamp(),
            $order->pigeon->rested_at->getTimestamp(),
            5
        );
    }

    /**
     * @dataProvider commonOrderData
     * @param array $commonOrderData
     * @return void
     */
    public function test_should_throw_when_order_status_is_not_placed(array $commonOrderData): void
    {
        $this->expectException(ModelNotFoundException::class);

        /** @var Pigeon */
        $pigeon = Pigeon::first();

        /** @var Order */
        $placedOrder = Order::create($commonOrderData + [
            'status' => OrderStatusEnum::COMPLETED,
            'pigeon_id' => $pigeon->id,
            'price' => $pigeon->cost * $commonOrderData['distance'],
        ]);

        $this->completedOrderAction->run($placedOrder->id);
    }

    /**
     * @param array $commonOrderData
     * @return void
     */
    public function test_should_throw_when_order_id_not_found(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->completedOrderAction->run(99);
    }
}
