<?php

namespace Tests\Feature\V1;

use App\Enums\OrderStatusEnum;
use App\Models\Order;
use App\Models\Pigeon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class CompletedOrderTest extends TestCase
{
    use RefreshDatabase;

    protected string $authorization;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $credential = Config::get('auth.basic');
        $this->authorization = 'Basic ' . base64_encode("{$credential['username']}:{$credential['password']}");
    }

    public function commonOrderData(): array
    {
        return [
            [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 500,
                    'deadline_at' => Carbon::now()->addHours(10),
                ],
            ]
        ];
    }

    /**
     * @dataProvider commonOrderData
     * @param array $commonOrderData
     * @return void
     */
    public function test_should_update_order_and_pigeon_status_when_current_order_status_is_placed(array $commonOrderData)
    {
        /** @var Pigeon */
        $pigeon = Pigeon::first();

        /** @var Order */
        $placedOrder = Order::create($commonOrderData + [
            'status' => OrderStatusEnum::PLACED,
            'pigeon_id' => $pigeon->id,
            'price' => $pigeon->cost * $commonOrderData['distance'],
        ]);

        $response = $this
            ->withHeader('Authorization', $this->authorization)
            ->patchJson("/v1/orders/{$placedOrder->id}/status/completed");
        $response->assertStatus(Response::HTTP_ACCEPTED);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'created_at',
                'updated_at',
                'pigeon' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'name',
                    'speed',
                    'range',
                    'cost',
                    'downtime',
                    'on_order',
                    'rested_at',
                ],
                'status',
                'customer_name',
                'customer_phone',
                'distance',
                'deadline_at',
                'price',
            ]
        ]);
        $this->assertEquals(OrderStatusEnum::COMPLETED, $response->json('data.status'));
    }

    /**
     * @dataProvider commonOrderData
     * @param array $commonOrderData
     * @return void
     */
    public function test_should_throw_when_current_order_status_is_not_placed(array $commonOrderData)
    {
        /** @var Pigeon */
        $pigeon = Pigeon::first();

        /** @var Order */
        $placedOrder = Order::create($commonOrderData + [
            'status' => OrderStatusEnum::REJECTED,
            'pigeon_id' => $pigeon->id,
            'price' => $pigeon->cost * $commonOrderData['distance'],
        ]);

        $response = $this
            ->withHeader('Authorization', $this->authorization)
            ->patchJson("/v1/orders/{$placedOrder->id}/status/completed");
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @return void
     */
    public function test_should_throw_when_current_order_not_found()
    {
        $response = $this
            ->withHeader('Authorization', $this->authorization)
            ->patchJson("/v1/orders/99/status/completed");
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
