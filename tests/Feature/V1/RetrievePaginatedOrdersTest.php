<?php

namespace Tests\Feature\V1;

use App\Contracts\DataTransferObjects\RetrievePaginatedOrdersQueryDto;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RetrievePaginatedOrdersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        Order::factory(10)->create();
    }

    /**
     * @dataProvider sampleDtos
     * @param RetrievePaginatedOrdersQueryDto $retrievePaginatedOrdersQueryDto
     * @return void
     */
    public function test_should_return_paginated_orders(RetrievePaginatedOrdersQueryDto $retrievePaginatedOrdersQueryDto): void
    {
        $response = $this->getJson('/v1/orders?' . $retrievePaginatedOrdersQueryDto->toQueryParams());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'pigeon',
                    'status',
                    'customer_name',
                    'customer_phone',
                    'distance',
                    'deadline_at',
                    'price',
                ],
            ],
            'links',
            'meta',
        ]);
        $this->assertEquals($retrievePaginatedOrdersQueryDto->getPage(), $response->json('meta.current_page'));
        $this->assertEquals($retrievePaginatedOrdersQueryDto->getPerPage(), $response->json('meta.per_page'));

        if ($retrievePaginatedOrdersQueryDto->getStatus() && count($response->json('data.*.id')) > 0) {
            $this->assertEquals($retrievePaginatedOrdersQueryDto->getStatus(), $response->json('data.*.status')[0]);
        }
    }

    /**
     * @return array
     */
    public function sampleDtos(): array
    {
        return [
            'page 1, per_page 2, and status placed' => [
                (new RetrievePaginatedOrdersQueryDto())
                    ->setPage(1)
                    ->setPerPage(2)
                    ->setStatus(OrderStatusEnum::PLACED)
            ],
            'page 2, per_page 3, and status rejected' => [
                (new RetrievePaginatedOrdersQueryDto())
                    ->setPage(2)
                    ->setPerPage(3)
                    ->setStatus(OrderStatusEnum::REJECTED)
            ],
            'default' => [
                new RetrievePaginatedOrdersQueryDto()
            ],
        ];
    }

    /**
     * @return void
     */
    public function test_should_return_paginated_orders_when_not_have_query_params(): void
    {
        $response = $this->getJson('/v1/orders');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'pigeon',
                    'status',
                    'customer_name',
                    'customer_phone',
                    'distance',
                    'deadline_at',
                    'price',
                ],
            ],
            'links',
            'meta',
        ]);
    }
}
