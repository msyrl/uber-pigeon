<?php

namespace Tests\Feature\V1;

use App\Enums\OrderStatusEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CreateOrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /**
     * @dataProvider acceptableData
     * @param array $acceptableData
     * @return void
     */
    public function test_should_return_placed_order_with_acceptable_data(array $acceptableData): void
    {
        $response = $this->postJson('/v1/orders', $acceptableData);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'created_at',
                'updated_at',
                'pigeon' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'name',
                    'speed',
                    'range',
                    'cost',
                    'downtime',
                    'on_order',
                    'rested_at',
                ],
                'status',
                'customer_name',
                'customer_phone',
                'distance',
                'deadline_at',
                'price',
            ]
        ]);
        $this->assertEquals(OrderStatusEnum::PLACED, $response->json('data.status'));
    }

    public function acceptableData(): array
    {
        return [
            [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 600,
                    'deadline_at' => Carbon::now()
                        ->addHours(9)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 500,
                    'deadline_at' => Carbon::now()
                        ->addHours(7)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 1000,
                    'deadline_at' => Carbon::now()
                        ->addHours(16)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 800,
                    'deadline_at' => Carbon::now()
                        ->addHours(12)
                        ->toIso8601String(),
                ],
            ]
        ];
    }

    /**
     * @dataProvider nonAcceptableData
     * @param array $nonAcceptableData
     * @return void
     */
    public function test_should_return_rejected_order_with_non_acceptable_data(array $nonAcceptableData): void
    {
        $response = $this->postJson('/v1/orders', $nonAcceptableData);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'created_at',
                'updated_at',
                'status',
                'customer_name',
                'customer_phone',
                'distance',
                'deadline_at',
            ]
        ]);
        $this->assertEquals(OrderStatusEnum::REJECTED, $response->json('data.status'));
    }

    public function nonAcceptableData(): array
    {
        return [
            [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 600,
                    'deadline_at' => Carbon::now()
                        ->addHours(7)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 500,
                    'deadline_at' => Carbon::now()
                        ->addHours(6)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 1000,
                    'deadline_at' => Carbon::now()
                        ->addHours(15)
                        ->toIso8601String(),
                ],
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 800,
                    'deadline_at' => Carbon::now()
                        ->addHours(11)
                        ->toIso8601String(),
                ],
            ]
        ];
    }

    /**
     * @dataProvider invalidData
     * @param array $invalidData
     * @return void
     */
    public function test_should_throw_when_data_is_invalid(array $invalidData)
    {
        $response = $this->postJson('/v1/orders', $invalidData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function invalidData(): array
    {
        return [
            'customer_name can\'t empty' => [
                [
                    'customer_phone' => '6281123123123',
                    'distance' => 600,
                    'deadline_at' => Carbon::now()
                        ->addHours(9)
                        ->toIso8601String(),
                ],
            ],
            'customer_phone can\'t empty' => [
                [
                    'customer_name' => 'John Doe',
                    'distance' => 600,
                    'deadline_at' => Carbon::now()
                        ->addHours(9)
                        ->toIso8601String(),
                ],
            ],
            'distance can\'t empty' => [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'deadline_at' => Carbon::now()
                        ->addHours(9)
                        ->toIso8601String(),
                ],
            ],
            'deadline_at can\'t empty' => [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 600,
                ],
            ],
            'deadline_at format not meet ISO-8601' => [
                [
                    'customer_name' => 'John Doe',
                    'customer_phone' => '6281123123123',
                    'distance' => 600,
                    'deadline_at' => Carbon::now()
                        ->addHours(9)
                        ->format('Y-m-d H:i:s'),
                ],
            ],
        ];
    }
}
