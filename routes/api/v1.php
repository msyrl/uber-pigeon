<?php

use App\Http\Middleware\BasicAuth;
use Illuminate\Support\Facades\Route;

Route::get('/orders', \App\Http\Controllers\V1\Order\IndexController::class);
Route::post('/orders', \App\Http\Controllers\V1\Order\StoreController::class);
Route::patch('/orders/{orderId}/status/completed', \App\Http\Controllers\V1\Order\Status\Completed\UpdateController::class)
    ->middleware(BasicAuth::class);
