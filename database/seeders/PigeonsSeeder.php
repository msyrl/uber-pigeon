<?php

namespace Database\Seeders;

use App\Models\Pigeon;
use Illuminate\Database\Seeder;

class PigeonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            [
                'name' => 'Antonio',
                'speed' => 70,
                'range' => 600,
                'cost' => 2,
                'downtime' => 2,
            ],
            [
                'name' => 'Bonito',
                'speed' => 80,
                'range' => 500,
                'cost' => 2,
                'downtime' => 3,
            ],
            [
                'name' => 'Carillo',
                'speed' => 65,
                'range' => 1000,
                'cost' => 2,
                'downtime' => 3,
            ],
            [
                'name' => 'Alejandro',
                'speed' => 70,
                'range' => 800,
                'cost' => 2,
                'downtime' => 2,
            ],
        ];

        foreach ($seeds as $seed) {
            if (!Pigeon::whereName($seed['name'])->exists()) {
                Pigeon::create($seed);
            }
        }
    }
}
