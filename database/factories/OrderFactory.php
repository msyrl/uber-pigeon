<?php

namespace Database\Factories;

use App\Enums\OrderStatusEnum;
use App\Models\Pigeon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $distance = $this->faker->numberBetween(100, 500);
        /** @var Carbon */
        $deadlineAt = $this->faker->randomElement([
            Carbon::now()->addHours(7),
            Carbon::now()->subDays(3),
        ]);

        $attributes = [
            'customer_name' => $this->faker->lastName(),
            'customer_phone' => '6281' . $this->faker->randomNumber(9),
            'distance' => $distance,
            'deadline_at' => $deadlineAt->toIso8601String(),
        ];

        $status = $deadlineAt->getTimestamp() >= Carbon::now()->getTimestamp()
            ? OrderStatusEnum::PLACED
            : OrderStatusEnum::REJECTED;

        if ($status === OrderStatusEnum::REJECTED) {
            return $attributes + [
                'status' => $status,
            ];
        }

        /** @var Pigeon */
        $pigeon = Pigeon::inRandomOrder()->first();
        $price = $pigeon->cost * $distance;

        return $attributes + [
            'status' => $status,
            'pigeon_id' => $pigeon->id,
            'price' => $price,
        ];
    }
}
