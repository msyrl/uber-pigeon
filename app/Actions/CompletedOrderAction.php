<?php

namespace App\Actions;

use App\Contracts\Actions\CompletedOrderAction as Contract;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CompletedOrderAction implements Contract
{
    public function run(int $orderId): Order
    {
        /** @var Order */
        $order = Order::query()
            ->whereId($orderId)
            ->whereStatus(OrderStatusEnum::PLACED)
            ->firstOrFail();

        DB::beginTransaction();

        $order->update(['status' => OrderStatusEnum::COMPLETED]);
        $this->updatePigeonStatus($order);

        DB::commit();

        return $order;
    }

    private function updatePigeonStatus(Order $order): void
    {
        $order->pigeon->update([
            'on_order' => false,
            'rested_at' => Carbon::now(),
        ]);
    }
}
