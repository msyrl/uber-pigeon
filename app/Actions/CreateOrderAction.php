<?php

namespace App\Actions;

use App\Contracts\Actions\CreateOrderAction as Contract;
use App\Contracts\DataTransferObjects\CreateOrderDto;
use App\Enums\OrderStatusEnum;
use App\Models\Order;
use App\Models\Pigeon;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CreateOrderAction implements Contract
{
    public function run(CreateOrderDto $createOrderDto): Order
    {
        /** @var Pigeon */
        $pigeon = $this->findAvailablePigeon($createOrderDto->distance, $createOrderDto->deadline_at);

        if (is_null($pigeon)) {
            return Order::create($createOrderDto->toArray() + [
                'status' => OrderStatusEnum::REJECTED,
            ]);
        }

        DB::beginTransaction();

        /** @var Order */
        $order = Order::create($createOrderDto->toArray() + [
            'pigeon_id' => $pigeon->id,
            'status' => OrderStatusEnum::PLACED,
            'price' => $this->calculatePrice($pigeon, $createOrderDto->distance),
        ]);

        $pigeon->update(['on_order' => true]);

        $order->load(['pigeon']);

        DB::commit();

        return $order;
    }

    private function findAvailablePigeon(int $distance, Carbon $deadlineAt): ?Pigeon
    {
        return Pigeon::query()
            ->where('range', '>=', $distance)
            ->orderByDesc('range')
            ->get()
            ->first(function (Pigeon $pigeon) use ($distance, $deadlineAt) {
                return $this->isPigeonTakeOrder(
                    $pigeon,
                    $distance,
                    $deadlineAt
                );
            });
    }

    private function isPigeonTakeOrder(
        Pigeon $pigeon,
        int $distance,
        Carbon $deadlineAt
    ): bool {
        if (!$pigeon->isAvailable()) {
            return false;
        }

        $time = $distance / $pigeon->speed;
        $arrivalAt = Carbon::now()->addSeconds($time * 3600);

        return $deadlineAt->getTimestamp() >= $arrivalAt->getTimestamp();
    }

    private function calculatePrice(Pigeon $pigeon, int $distance): int
    {
        return $pigeon->cost * $distance;
    }
}
