<?php

namespace App\Actions;

use App\Contracts\Actions\RetrievePaginatedOrdersAction as Contract;
use App\Contracts\DataTransferObjects\RetrievePaginatedOrdersQueryDto;
use App\Models\Order;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class RetrievePaginatedOrdersAction implements Contract
{
    public function run(RetrievePaginatedOrdersQueryDto $retrieveOrdersQueryDto): LengthAwarePaginator
    {
        return Order::with(['pigeon'])
            ->when($retrieveOrdersQueryDto->getStatus(), function ($query) use ($retrieveOrdersQueryDto) {
                $query->where('status', $retrieveOrdersQueryDto->getStatus());
            })
            ->paginate(
                $retrieveOrdersQueryDto->getPerPage(),
                ['*'],
                'page',
                $retrieveOrdersQueryDto->getPage()
            );
    }
}
