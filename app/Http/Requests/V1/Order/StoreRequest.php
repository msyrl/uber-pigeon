<?php

namespace App\Http\Requests\V1\Order;

use App\Contracts\DataTransferObjects\CreateOrderDto;
use DateTime;
use DateTimeInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => [
                'required',
                'string',
            ],
            'customer_phone' => [
                'required',
                'string',
            ],
            'distance' => [
                'required',
                'integer',
            ],
            'deadline_at' => [
                'required',
                'date_format:' . DateTimeInterface::ATOM,
            ],
        ];
    }

    public function toDto(): CreateOrderDto
    {
        return new CreateOrderDto(
            strval($this->input('customer_name')),
            strval($this->input('customer_phone')),
            intval($this->input('distance')),
            new Carbon(strval($this->input('deadline_at')))
        );
    }
}
