<?php

namespace App\Http\Controllers\V1\Order\Status\Completed;

use App\Contracts\Actions\CompletedOrderAction;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\OrderResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateController extends Controller
{
    private CompletedOrderAction $completedOrderAction;

    public function __construct(CompletedOrderAction $completedOrderAction)
    {
        $this->completedOrderAction = $completedOrderAction;
    }

    /**
     * Handle the incoming request.
     *
     * @param int $orderId
     * @return JsonResponse
     */
    public function __invoke(int $orderId)
    {
        $order = $this->completedOrderAction->run($orderId);

        return (new OrderResource($order))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }
}
