<?php

namespace App\Http\Controllers\V1\Order;

use App\Contracts\Actions\RetrievePaginatedOrdersAction;
use App\Contracts\DataTransferObjects\RetrievePaginatedOrdersQueryDto;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\OrderResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class IndexController extends Controller
{
    private RetrievePaginatedOrdersAction $retrievePagiantedOrdersAction;

    public function __construct(RetrievePaginatedOrdersAction $retrievePagiantedOrdersAction)
    {
        $this->retrievePagiantedOrdersAction = $retrievePagiantedOrdersAction;
    }

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function __invoke(Request $request): AnonymousResourceCollection
    {
        $dto = new RetrievePaginatedOrdersQueryDto();

        if ($request->filled('page')) {
            $dto->setPage($request->get('page'));
        }

        if ($request->filled('per_page')) {
            $dto->setPerPage($request->get('per_page'));
        }

        if ($request->filled('status')) {
            $dto->setStatus($request->get('status'));
        }

        $orders = $this->retrievePagiantedOrdersAction->run($dto);

        return OrderResource::collection($orders);
    }
}
