<?php

namespace App\Http\Controllers\V1\Order;

use App\Contracts\Actions\CreateOrderAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Order\StoreRequest;
use App\Http\Resources\V1\OrderResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class StoreController extends Controller
{
    private CreateOrderAction $createOrderAction;

    public function __construct(CreateOrderAction $createOrderAction)
    {
        $this->createOrderAction = $createOrderAction;
    }

    /**
     * Handle the incoming request.
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function __invoke(StoreRequest $request): JsonResponse
    {
        $order = $this->createOrderAction->run($request->toDto());

        return (new OrderResource($order))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
}
