<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property ?Carbon $created_at
 * @property ?Carbon $updated_at
 * @property string $name
 * @property int $speed
 * @property int $range
 * @property int $cost
 * @property int $downtime
 * @property bool $on_order
 * @property ?Carbon $rested_at
 */
class Pigeon extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'speed',
        'range',
        'cost',
        'downtime',
        'on_order',
        'rested_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'speed' => 'integer',
        'range' => 'integer',
        'cost' => 'integer',
        'downtime' => 'integer',
        'on_order' => 'boolean',
        'rested_at' => 'datetime',
    ];

    /**
     * Determine is pigeon available for order
     *
     * @return bool
     */
    public function isAvailable(): bool
    {
        if ($this->on_order) {
            return false;
        }


        if (empty($this->rested_at)) {
            return true;
        }

        return Carbon::now()->diffInHours($this->rested_at) >= $this->downtime;
    }
}
