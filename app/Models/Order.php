<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property ?Carbon $created_at
 * @property ?Carbon $updated_at
 * @property int $pigeon_id
 * @property string $status
 * @property string $customer_name
 * @property string $customer_phone
 * @property int $distance
 * @property Carbon $deadline_at
 * @property ?int $price
 * @property-read ?Pigeon $pigeon
 */
class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'pigeon_id',
        'status',
        'customer_name',
        'customer_phone',
        'distance',
        'deadline_at',
        'price',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'distance' => 'integer',
        'deadline_at' => 'datetime',
        'price' => 'integer',
    ];

    public function pigeon(): BelongsTo
    {
        return $this->belongsTo(Pigeon::class)->withDefault(null);
    }
}
