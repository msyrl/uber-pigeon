<?php

namespace App\Providers;

use App\Actions\CompletedOrderAction as CompletedOrderActionImplementation;
use App\Actions\CreateOrderAction as CreateOrderActionImplementation;
use App\Actions\RetrievePaginatedOrdersAction as RetrievePaginatedOrdersActionImplementation;
use App\Contracts\Actions\CompletedOrderAction;
use App\Contracts\Actions\CreateOrderAction;
use App\Contracts\Actions\RetrievePaginatedOrdersAction;
use Illuminate\Support\ServiceProvider;

class ActionServiceProvider extends ServiceProvider
{
    public $bindings = [
        CreateOrderAction::class => CreateOrderActionImplementation::class,
        CompletedOrderAction::class => CompletedOrderActionImplementation::class,
        RetrievePaginatedOrdersAction::class => RetrievePaginatedOrdersActionImplementation::class,
    ];
}
