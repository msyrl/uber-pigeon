<?php

namespace App\Enums;

class OrderStatusEnum
{
    const REJECTED = 'rejected';
    const PLACED = 'placed';
    const COMPLETED = 'completed';
}
