<?php

namespace App\Contracts\Actions;

use App\Contracts\DataTransferObjects\RetrievePaginatedOrdersQueryDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface RetrievePaginatedOrdersAction
{
    public function run(RetrievePaginatedOrdersQueryDto $retrieveOrdersQueryDto): LengthAwarePaginator;
}
