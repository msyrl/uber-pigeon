<?php

namespace App\Contracts\Actions;

use App\Contracts\DataTransferObjects\CreateOrderDto;
use App\Models\Order;

interface CreateOrderAction
{
    public function run(CreateOrderDto $createOrderDto): Order;
}
