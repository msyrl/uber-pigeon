<?php

namespace App\Contracts\Actions;

use App\Models\Order;

interface CompletedOrderAction
{
    public function run(int $orderId): Order;
}
