<?php

namespace App\Contracts\DataTransferObjects;

use Illuminate\Support\Carbon;

class CreateOrderDto
{
    public string $customer_name;

    public string $customer_phone;

    public int $distance;

    public Carbon $deadline_at;

    public function __construct(
        string $customer_name,
        string $customer_phone,
        int $distance,
        Carbon $deadline_at
    ) {
        $this->customer_name = $customer_name;
        $this->customer_phone = $customer_phone;
        $this->distance = $distance;
        $this->deadline_at = $deadline_at;
    }

    public function toArray(): array
    {
        return [
            'customer_name' => $this->customer_name,
            'customer_phone' => $this->customer_phone,
            'distance' => $this->distance,
            'deadline_at' => $this->deadline_at,
        ];
    }
}
