<?php

namespace App\Contracts\DataTransferObjects;

class RetrievePaginatedOrdersQueryDto
{
    private ?int $page = 1;

    private ?int $per_page = 10;

    private ?string $status = null;

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPerPage(int $per_page): self
    {
        $this->per_page = $per_page;

        return $this;
    }

    public function getPerPage(): int
    {
        return $this->per_page;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function toQueryParams(): string
    {
        return http_build_query([
            'page' => $this->getPage(),
            'per_page' => $this->getPerPage(),
            'status' => $this->getStatus(),
        ]);
    }
}
