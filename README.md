## UberPigeon

#### Requirements

- PHP >= 7.4
- Composer
- MySQL or PostgreSQL

#### Preinstalation

1. Run command `copy .env.example .env` on your terminal.
2. Customize enviroments variables which you need especially database connection config.

#### Instalation

```bash
composer install
php artisan key:generate
php artisan migrate --seed
php artisan serve
```

#### API Documentation

Available at `/docs`

#### Sample Orders

If you want to create sample orders please run `php artisan db:seed --class=SampleOrdersSeeder`

#### Testing

```bash
php artisan test
```
